package com.company;

import java.util.Scanner;

public class Film {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Напишите количество парковочных мест:  ");
        int PlacesOnParking = scanner.nextInt();
        System.out.print("Напишите количество парковочных мест для грузовиков: ");
        int truckPlacesOnParking = scanner.nextInt();
        Parking parking = new Parking(PlacesOnParking, truckPlacesOnParking);
        Manager simulation = new Manager();
        simulation.setParking(parking);

        simulation.runMainCycle();
    }
}
