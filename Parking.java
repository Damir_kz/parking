package com.company;

public class Parking {
    private final int DEFAULT_PLACES;
    private final int TRUCK_PLACES;
    private Cars[] defaultCars;
    private Truck[] trucks;

    public Parking(int defaultPlaces, int truckPlaces) {
        this.DEFAULT_PLACES = defaultPlaces;
        this.TRUCK_PLACES = truckPlaces;
        defaultCars = new Cars[this.DEFAULT_PLACES];
        trucks = new Truck[this.TRUCK_PLACES];
    }

    public int getDefaultPlaces() {
        return DEFAULT_PLACES;
    }

    public int getTruckPlaces() {return TRUCK_PLACES;}

    public int showFreeDefaultPlace() {
        for (int i = 0; i < defaultCars.length; i++) {
            if (defaultCars[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public int showFreeTruckPlace() {
        for (int i = 0; i < trucks.length; i++) {
            if (trucks[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public void setDefaultCars(int place, Cars car) {
        defaultCars[place] = car;
    }

    public void setDefaultCars(Cars[] defaultCars) {
        this.defaultCars = defaultCars;
    }

    public void setTrucks(Truck[] trucks) {
        this.trucks = trucks;
    }

    public void setTrucks(int place, Truck truck) {
        trucks[place] = truck;
    }

    public int getAllPlaces(){
        return TRUCK_PLACES + DEFAULT_PLACES;
    }

    public Cars[] getDefaultCars() {
        return defaultCars;
    }

    public Truck[] getTrucks() {return trucks;}

    public void resetParking() {
        defaultCars = new Car[this.DEFAULT_PLACES];
        trucks = new Truck[this.TRUCK_PLACES];
    }

    public int[] showFreePlacesForTrucksOnDefaultParking(){
        int firstPlace = -1;
        int secondPlace = -1;

        for (int i = 0; i < defaultCars.length; i++) {
            if (defaultCars[i] == null & firstPlace == -1) {
                firstPlace = i;
            }
            if(defaultCars[i] == null & firstPlace != -1 & secondPlace == -1){
                secondPlace = i;
            }
        }
        return new int[]{firstPlace, secondPlace};
    }
}
