package com.company;

import java.util.Random;
import java.util.Scanner;

public class Manager {
    private static final String RED_COLOR = "\u001B[31m";
    private static final String GREEN_COLOR = "\u001B[32m";
    private static final String BLUE_COLOR = "\u001B[34m";
    private static final String RESET_COLOR = "\u001B[0m";
    private final Scanner SCANNER;
    private boolean isWorking;
    private final Random random;
    private Parking parking;
    private int carId;


    public Manager() {
        random = new Random();
        isWorking = true;
        SCANNER = new Scanner(System.in);
        carId = 1000;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }

    public void runMainCycle() {
        while (isWorking) {
            Cars[] cars = parking.getDefaultCars();
            Truck[] trucks = parking.getTrucks();

            for (int i = 0; i < cars.length; i++) {
                if (cars[i] != null) {
                    cars[i].subtractOneMove();
                    if (cars[i].getMoves() <= 0) {
                        cars[i] = null;
                    }
                }
            }

            for (int i = 0; i < trucks.length; i++) {
                if (trucks[i] != null) {
                    trucks[i].subtractOneMove();
                    if (trucks[i].getMoves() <= 0) {
                        trucks[i] = null;
                    }
                }
            }

            parking.setDefaultCars(cars);
            parking.setTrucks(trucks);


            int carsNow = random.nextInt((parking.getAllPlaces() / 3) + 1);

            for (int i = 0; i < carsNow; i++) {
                int move = random.nextInt(9) + 1;
                int carType = random.nextInt(2);

                if (carType == 1) {
                    int truckPlace = parking.showFreeTruckPlace();
                    Truck car = new Truck(move, carId);

                    if (truckPlace > -1) {
                        parking.setTrucks(truckPlace, car);
                    } else {
                        int[] takenPlacesOnDefaultParking = parking.showFreePlacesForTrucksOnDefaultParking();
                        if (takenPlacesOnDefaultParking[0] > -1 & takenPlacesOnDefaultParking[1] > -1) {
                            parking.setDefaultCars(takenPlacesOnDefaultParking[0], car);
                            parking.setDefaultCars(takenPlacesOnDefaultParking[1], car);
                        } else {
                            int shortestMove = countShortestMoves(cars);
                            System.out.println(RED_COLOR + "Свободных мест нет, пожалуйста подождите " + shortestMove + " Движение" + RESET_COLOR);
                            break;
                        }
                    }

                } else {
                    int defaultPlace = parking.showFreeDefaultPlace();
                    Car car = new Car(move, carId);

                    if (defaultPlace > -1) {
                        parking.setDefaultCars(defaultPlace, car);
                    } else {
                        int shortestMove = countShortestMoves(cars);
                        System.out.println(RED_COLOR + "Свободных мест нет, пожалуйста подождите " + shortestMove + "Движение" + RESET_COLOR);
                        break;
                    }
                }

                carId++;
            }

            System.out.println(BLUE_COLOR + "q - выйти; n - следующий ход; c - свободная парковка ; i - показать информацию" + RESET_COLOR);
            String message = SCANNER.nextLine();

            if (message.toLowerCase().equals("i")) {
                showInformation(cars);
                message = SCANNER.nextLine();
                while (message.toLowerCase().equals("i")) {
                    showInformation(cars);
                    message = SCANNER.nextLine();
                }
            }

            while (!isAvailableMessage(message.toLowerCase())) {
                System.out.print(RED_COLOR + "Я не знаю эту инструкцию! Попробуйте еще раз: " + RESET_COLOR);
                message = SCANNER.nextLine();
            }

            makeMessage(message.toLowerCase());

        }

    }

    private boolean isAvailableMessage(String message) {
        String[] availableMessages = new String[]{"n", "q", "c", "i"};

        return isInMassive(availableMessages, message);
    }

    private boolean isInMassive(String[] massive, String element) {
        for (String s : massive) {
            if (s.equals(element)) {
                return true;
            }
        }
        return false;
    }

    private void makeMessage(String message) {
        switch (message) {
            case "q" -> {
                System.out.println(RED_COLOR + "Вы завершили моделирование" + RESET_COLOR);
                isWorking = false;
            }
            case "n" -> {
                System.out.println(BLUE_COLOR + "Продолжение" + RESET_COLOR);
                isWorking = true;
            }
            case "c" -> {
                System.out.println(GREEN_COLOR + "Все машины уехали с парковки " + RESET_COLOR);
                parking.resetParking();
            }
            case "i" -> {
                Cars[] cars = parking.getDefaultCars();
                System.out.println(GREEN_COLOR + "Самое небольшое передвижение : " + countShortestMoves(cars) + "\n" +
                        "Свободные  места: " + countFreePlaces(cars) + "\n" +
                        "Заняты места: " + countTakenPlaces(cars));
            }
        }
    }

    private int countShortestMoves(Cars[] cars) {
        int shortestMove = 10;

        for (Cars car : cars) {
            if (car != null) {
                if (car.getMoves() < shortestMove) {
                    shortestMove = car.getMoves();
                }
            }
        }

        return shortestMove;
    }

    private int countFreePlaces(Cars[] cars) {
        int freePlaces = 0;
        for (Cars car : cars) {
            if (car == null) {
                freePlaces++;
            }
        }
        return freePlaces;
    }

    private int countTakenPlaces(Cars[] cars) {
        return parking.getDefaultPlaces() - countFreePlaces(cars);
    }

    private void showInformation(Cars[] cars) {
        System.out.print(GREEN_COLOR + "Самое небольшое движение: " + countShortestMoves(cars) + "\n" +
                "Свободные места: " + countFreePlaces(cars) + "\n" +
                "Места заняты: " + countTakenPlaces(cars) + "\n" + RESET_COLOR + BLUE_COLOR +
                "Напишите другую инструкцию: " + RESET_COLOR);

    }

}
