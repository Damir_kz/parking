package com.company;

public class Cars {
    private int moves;
    private final int ID;

    public Cars(int moves, int id) {
        this.moves = moves;
        this.ID = id;
    }

    public void subtractOneMove() {
        moves--;
    }

    public int getId() {
        return ID;
    }

    public int getMoves() {
        return moves;
    }
}
